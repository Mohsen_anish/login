package com.example.testlogin.api

import com.example.testlogin.models.LoginRequest
import com.example.testlogin.models.LoginResponse
import com.example.testlogin.models.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
interface ApiInterFace {
@POST("login")
fun login(@Body loginrequest:LoginRequest):Call<User>


@GET("apikey")
fun apikey(@Header("Authorization")apikey:String):Callback<LoginResponse>
}