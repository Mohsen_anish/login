package com.example.testlogin.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitMaker {
    companion object{
        val retrofit: Retrofit?=null
        val api:ApiInterFace?=null
        fun builderretrofit():Retrofit{
            if (retrofit!=null){
                return retrofit
            }else{
                return Retrofit.Builder()
                    .baseUrl("https://www.poushka.com/api/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            }

        }
        fun getapi():ApiInterFace{
            if (api!=null){
                return api
            }else{
                return builderretrofit().create(ApiInterFace::class.java)
            }
        }

    }
}