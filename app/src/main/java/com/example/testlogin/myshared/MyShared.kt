package com.example.testlogin.myshared

import android.content.Context
import android.content.SharedPreferences
import android.view.Display

class MyShared(val context:Context) {

    lateinit var sharedPreferences: SharedPreferences
    lateinit var editor:SharedPreferences.Editor

    init {
        sharedPreferences=context.getSharedPreferences("app",Context.MODE_PRIVATE)
        editor=sharedPreferences.edit()
    }
    fun saveapikey(apikey:String){

        editor.putString("apikey",apikey)
        editor.apply()


    }
    fun getapikey():String{
        val apikey=sharedPreferences.getString("apikey","")
        return apikey!!

    }
    fun savelogin(islogin:Boolean){
        editor.putBoolean("login",islogin)
        editor.apply()
    }
    fun getlogin():Boolean{
        val login=sharedPreferences.getBoolean("login",false)
        return login
    }


    
}