package com.example.testlogin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*
import kotlin.collections.ArrayList


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class HomeFragment : Fragment() {
    val fragments=ArrayList<Fragment>()
    val names=ArrayList<String>(Arrays.asList("ورود به حساب","صندوق پیام ها"))


    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragments.add(LoginFragment())
        fragments.add(InboxFragment())
        val adapter=ScreenSliderAdapter(requireActivity(),fragments)
        viewpager2.adapter=adapter
        TabLayoutMediator(tab_home,viewpager2){tab, position ->
            tab.setText(names.get(position))
        }.attach()

       // tab_home.selectTab(tab_home.getTabAt(0))



    }

    fun changeTab(position: Int)  {
        tab_home.selectTab(tab_home.getTabAt(position))
    }

    private inner class ScreenSliderAdapter(fa:FragmentActivity,val fragments:ArrayList<Fragment>):FragmentStateAdapter(fa) {
        override fun getItemCount(): Int {
        return    fragments.size
        }

        override fun createFragment(position: Int): Fragment {
        return    fragments[position]

        }
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}